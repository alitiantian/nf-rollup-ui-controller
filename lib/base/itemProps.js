
/**
 * 基础控件的共用属性 propsItem
 */
export default {
  columnId: { // 101 控件ID，必填
    type: Number,
    default: () => Math.floor((Math.random() * 10000000) + 1) // new Date().valueOf()
  },
  model: Object, // 表单的 model，可以整体传入。
  colName: String, // 102 字段名称，也是 model 的属性名称
  controlType: Number, // 104 控件类型编号，必填，识别表单子控件的类型
  events: {
    type: Object,
    default: () => {
      return {
        input: () => {}, // input 事件
        enter: () => {}, // 按了回车
        keydown: () => {} // 正在输入
      }
    }
  },
  delay: { // 防抖的时间间隔，0：不用防抖。
    type: Number,
    default: 0
  },
  size: { // 109 medium / small / mini 三选一，不必填
    type: String,
    default: 'mini',
    validator: (value) => {
      // 这个值必须匹配下列字符串中的一个
      return ['medium', 'small', 'mini'].indexOf(value) !== -1
    }
  },
  optionList: { // 备用选项
    type: Array,
    default: () => []
  },
  clearable: { // 统一设置，是否可清空，默认可以清空
    type: Boolean,
    default: true
  },
  validate_event: { // 统一设置，不必填，输入时是否触发表单的校验
    type: Boolean,
    default: true
  },
  show_word_limit: { // 统一设置，不必填，是否显示输入字数统计 text和area有效
    type: Boolean,
    default: true
  }
}