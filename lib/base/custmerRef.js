import { customRef } from 'vue'

const isNumber = (val) => {

}

/**
 * 控件的直接输入，不用防抖
 * @param { object } props 组件的 props
 * @param { object } events 事件集合，run：立即提交；clear：清空计时，用于汉字输入
 * @param { object } emit 组件的 emit
 * @param { string } name v-model的名称，默认 modelValue，用于emit
 */
const _customRef = (props, emit = () => {}, name = 'modelValue') => {
  
  return customRef((track, trigger) => {
    return {
      get () {
        track()
        if (typeof props.model === 'object') {
          return props.model[props.colName]
        } else {
          return props[name]
        }
      },
      set (val) {
        trigger()
        if (typeof props.model === 'object') {
          props.model[props.colName] = val
        } else {
          emit(`update:${name}`, val)
        }
      }
    }
  })
}

export default _customRef
