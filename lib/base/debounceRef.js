import { customRef, watch } from 'vue'

/**
 * 控件的防抖输入
 * @param { object } props 组件的 props
 * @param { object } events 事件集合，run：立即提交；clear：清空计时，用于汉字输入
 * @param { object } emit 组件的 emit
 * @param { number } delay 延迟时间，默认500毫秒
 * @param { string } name v-model的名称，默认 modelValue，用于emit
 */
const debounceRef = (props, events = {}, emit = () => {}, delay = 500, name = 'modelValue') => {
  // 计时器
  let timeout = null

  // 内部变量，直接给控件赋值。
  // 初始化设置属性值
  let _value = (typeof props.model === 'object') ? 
    props.model[props.colName] :
    props[name]
  
  // 提交给属性
  const _setValue = (__val) => {
    if (typeof props.model === 'object') {
      props.model[props.colName] = __val
    } else {
      emit(`update:${name}`, __val)
    }
  }

  /**
   * 立即提交的事件
   */
  events.run = () => {
    clearTimeout(timeout) // 清掉上一次的计时
    _setValue(_value)
  }

  /**
   * 清掉上次计时，用户汉字的连续输入
   */
  events.clear = () => {
    clearTimeout(timeout) // 清掉上一次的计时
  }
  
  return customRef((track, trigger) => {
    // 监听父组件的属性变化，然后赋值
    if (typeof props.model === 'object') {
      watch(() => props.model[props.colName], (v1) => {
        _value = v1
        trigger()
      })
    } else {
      watch(() => props[name], (v1) => {
        _value = v1
        trigger()
      })
    }

    return {
      get () {
        track()
        return _value
      },
      set (val) {
        _value = val // 绑定值
        trigger() // 输入内容绑定到控件，但是不提交
        clearTimeout(timeout) // 清掉上一次的计时
        timeout = setTimeout(() => {
          _setValue(val) // 提交
        }, delay)
      }
    }
  })
}

export default debounceRef
