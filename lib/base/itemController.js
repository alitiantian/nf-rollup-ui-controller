import { watch } from 'vue'
// 基础控件的控制
import debounceRef from './debounceRef.js'
import custmerRef from './custmerRef.js'
/**
 * 表单子控件的控制类
 * @param {*} props 组件的 props
 * @param {*} emit 组件的 emit
 * @returns 
 */
export default function itemController (props, emit) {
  // 属性里的事件集合
  const events = props.events

  // 防抖的相关事件
  const ctlEvents = {
    run: () => {}, // 立即执行，不防抖了
    clear: () => {} // 清除上一次计时，用于输入汉字的情况。
  }
  // v-model 的中转站，可以应对各种情况
  const value = props.delay === 0 ?
    custmerRef(props, emit) // 不用防抖，直接用 属性
    : debounceRef(props, ctlEvents, emit, props.delay) // 需要用防抖
  
  // 不防抖，立即提交
  const run = () => {
    ctlEvents.run() // 立即执行
  }
  // 输入汉字的情况
  const clear = () => {
    ctlEvents.clear() // 输入汉字时清除上次计时
  }
  // 输入内容发生变化
  const myinput = () => {
     events.input() 
  }

  return {
    value,
    run,
    clear,
    myinput
  }
} 