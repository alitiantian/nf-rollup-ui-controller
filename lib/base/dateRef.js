import { customRef, watch } from 'vue'
import dayjs from 'dayjs'

/**
 * 日期的处理
 * @param { object } props 组件的 props
 * @param { object } events 事件集合，run：立即提交；clear：清空计时，用于汉字输入
 * @param { object } emit 组件的 emit
  * @param { string } name v-model的名称，默认 modelValue，用于emit
 */
const dateRef = (props, events = {}, emit = () => {}, name = 'modelValue') => {
  // 内部值，日期类型
  let _mydate = new Date()
  // 属性值，字符串或者日期类型
  let _propsValue = null

  // 提交给属性
  const _setValue = (__val) => {
    // 判断初始化格式，原生日期；格式化字符串
    let re = ''
    if (__val !== null) {
      switch (props.returnFormat) {
        case 'date':
          re = __val
          break
        case 'timestamp':
          re = __val.valueOf()
          break
        default:
          re = dayjs(__val).format(props.returnFormat)
          break
      }
    }
    if (typeof props.model === 'object') {
      props.model[props.colName] = re
    } else {
      emit(`update:${name}`, re)
    }
  }

  // 从属性里面取值
  const _getValue = (val) => {
    _propsValue = val
    if (typeof _propsValue === 'string' || typeof _propsValue === 'number') {
      if (_propsValue === '') {
        _mydate = null
      } else {
        if (_propsValue.toString().length === 4) {
          _mydate = new Date(_propsValue)
        } else if (_propsValue.toString() === (_propsValue * 1).toString()) {
          _mydate = new Date(_propsValue * 1)
        } else {
          _mydate = new Date(_propsValue)
        }
        // value.value = dayjs(mydate.value).format(props.returnFormat)
      }
    }
  }

  return customRef((track, trigger) => {
    // 监听父组件的属性变化，然后赋值
    if (typeof props.model === 'object') {
      _propsValue = props.model[props.colName]
      watch(() => props.model[props.colName], (v1) => {
        _getValue(v1)
        trigger()
      }, { immediate: true})
    } else {
      _propsValue = props[name]
      watch(() => props[name], (v1) => {
        _getValue(v1)
        trigger()
      }, { immediate: true})
    }

    return {
      get () {
        track()
        return _mydate
      },
      set (val) {
        _mydate = val // 绑定值
        trigger() // 输入内容绑定到控件，但是不提交
        _setValue(val) // 提交
      }
    }
  })
}

export default dateRef
