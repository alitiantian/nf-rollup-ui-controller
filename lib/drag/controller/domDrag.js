
/**
 * 把 dom 变成可以拖拽的形式，记录拖拽信息。
 * * 针对table的th，form 和 find 的div
 */
export default class domDrag {
  /**
   * 传入一个 dom，设置拖拽属性，触发拖拽事件
   * @param {*} _dom 要拖拽的dom，比如 table 的th，表单的div等
   */
  constructor (_dom, meta, dragInfo, colId, table) {
    this.dom = _dom
    this.align = () => {} // 设置对齐（grid专用）
    this.order = () => {} // 设置排序（共用）
    this.delDom = () => {} // 移除 dom
    // this.timeout = null // 移除的延迟
    // 设置 dom 可以拖拽
    this.dom.setAttribute('draggable', true)
    
    // 开始拖拽
    this.dom.ondragstart = null // 去掉以前加载的事件
    this.dom.ondragstart = (event) => {
      // event.dataTransfer.effectAllowed = 'move'
      // table.style.cursor = 'move'
      dragInfo.state = 'pending'
      dragInfo.sourceId = colId
      dragInfo.sourceIndex = meta.colOrder.findIndex(a => a === dragInfo.sourceId)
    }

    // 拖拽时经过
    this.dom.ondragover = null // 去掉以前加载的事件
    this.dom.ondragover = (event) => {
      event.preventDefault()
      clearTimeout(dragInfo.timeout)
      dragInfo.oldDom = event.target
      dragInfo.isLeft = event.offsetX < event.target.offsetWidth / 2

      // 判断状态
      if (event.ctrlKey) {
        event.target.style.border = "2px dotted #ec5367"
      } else {
        if (dragInfo.isLeft) {
          event.target.style.border = "2px dotted #eb982c"
        } else {
          event.target.style.border = "2px dotted #2140c9"
        }
      }
      
      // event.dataTransfer.dropEffect = 'link'
      // table.style.cursor = 'e-resize, move !important' // 改变光标形状

      // this.dom.style.color = '#123456'
      // event.target.style.border = "1px dotted red"
      // this.dom.style.border = "3px dotted red"
    }

    // 拖拽离开
    this.dom.ondragleave = null // 去掉以前加载的事件
    this.dom.ondragleave = (event) => {
      clearTimeout(dragInfo.timeout)
      dragInfo.oldDom.style.border = ""
      dragInfo.timeout = setTimeout(() => {
        if (dragInfo.state !== 'end') {
          this.delDom(dragInfo, event) // 调用移除事件
        }
      }, 1000)
    }

    // 结束拖拽
    this.dom.ondrop = null // 去掉以前加载的事件
    this.dom.ondrop = (event) => {
      // event.preventDefault()
      // event.target.style.opacity = "1";
      clearTimeout(dragInfo.timeout)
      dragInfo.oldDom.style.border = ""
      // event.dataTransfer.dropEffect = 'link'
      dragInfo.state = 'end'
      dragInfo.offsetX = event.offsetX
      dragInfo.isLeft = dragInfo.offsetX < event.target.offsetWidth / 2
      dragInfo.ctrl = event.ctrlKey
      dragInfo.targetId = colId // textToColumnId[event.target.innerText]
      dragInfo.targetIndex = meta.colOrder.findIndex(a => a === dragInfo.targetId)
      if (dragInfo.sourceId === dragInfo.targetId) {
        this.align(dragInfo, event) // 在同一个dom里拖拽，调整对齐
      } else {
        this.order(dragInfo, event) // 拖拽到另一个 dom，排序
      }
    }
    
  }

  // 设置对齐（grid），设置列数（form）
  setAlign(cb) {
    this.align = cb
  }
  // 设置排序（共用）
  setOrder(cb) {
    this.order = cb
  }
  // 移除 dom
  deleteDom(cb) {
    this.delDom = cb
  }
}
