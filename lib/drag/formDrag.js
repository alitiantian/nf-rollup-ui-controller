import { nextTick } from 'vue'

// 让 dom 可以拖拽
import domDrag from './controller/domDrag.js'
// 设置排序
import setOrder from './controller/set-order.js'

/**
 *  数据列表的拖拽相关的操作，记录调整后的td 的宽度，调整td的顺序，移除td
 * @param {*} meta 列表的 meta
 * @param {*} form 表单子控件 集合
 * @param {*} deleteCol 移除 th 的确认对话框
 * @returns 
 */
export default function formDrag(meta, form, deleteCol) {
  /**
   * 移除选择的字段
   * @param {*} meta 元数据
   * @param {*} dragInfo 拖拽信息
   */
  const _setRemove = (dragInfo) => {
    const col = meta.itemMeta[dragInfo.sourceId]
    // 调用外部的确认对话框
    deleteCol(col, () => {
      // 确认移除，才会执行回调
      meta.colOrder.splice(dragInfo.sourceIndex, 1)
      nextTick(() => {
        setup() // 重新绘制
      })
    })
  }

  /**
   * 设置 列数
   */
  const _setThAlgin = (meta, dragInfo) => {
    // 修改列数
    if (dragInfo.isLeft) {
      // 判断现在的列数
      if (meta.formColCount > 1) meta.formColCount --
    } else {
      if (meta.formColCount < 8) meta.formColCount ++
    }
  }
 
  /**
   * 列表的初始化设置
   */
  const setup = () => {
   
    // 拖拽信息
    const dragInfo = {
      timeout: null, // 设置拖拽出去的 setTimeout
      oldDom: {style:{border:''}}, // 记录之前的拖拽 dom
      state: '', // 拖拽状态，拖拽中（pending），拖拽结束（end）
      offsetX: 0,
      isLeft: true, // 是否在 th 的左侧结束拖拽，左侧或者右侧。
      ctrl: false, // 是否按下了ctrl
      sourceId: '', // 开始拖拽的th对应的字段ID
      targetId: '', // 结束拖拽的th对应的字段ID
      sourceIndex: 0, // 开始拖拽的位置
      targetIndex: 0 // 结束拖拽的位置
    }

    const formItem = Array.from(form.children)

    let i = 0
    // 遍历表单子控件
    formItem.forEach(element => {
      const label = element.children[0].children[0]
      const ctrl = element.children[0].children[1]
      const colId = meta.colOrder[i++]
      // 给 th 设置拖拽事件
      const thDrag = new domDrag(label, meta, dragInfo, colId, form)
      // 设置对齐
      thDrag.setAlign((_dragInfo) => {
        _setThAlgin(meta, _dragInfo)
      })
      // 设置排序
      thDrag.setOrder((_dragInfo) => {
        setOrder(meta, dragInfo) // 调整顺序、交换位置
        setTimeout(() => {
          setup() // 重新绘制
        }, 1000)
      })
      // 移除th
      thDrag.deleteDom((_dragInfo) => {
        _setRemove(dragInfo) // 移除
        
      })
    })
    document.ondrop = (event) => {
    }
  }

  return {
    setup // 列表设置拖拽
  }

}