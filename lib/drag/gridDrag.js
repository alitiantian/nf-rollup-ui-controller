import { nextTick } from 'vue'

// 让 dom 可以拖拽
import domDrag from './controller/domDrag.js'
// 设置排序
import setOrder from './controller/set-order.js'

/**
 *  数据列表的拖拽相关的操作，记录调整后的td 的宽度，调整td的顺序，移除td
 * @param {*} meta 列表的 meta
 * @param {*} table table 的 dom
 * @param {*} deleteTh 移除 th 的确认对话框
 * @returns 
 */
export default function gridDrag(meta, table, deleteTh) {
  /**
   * 移除选择的字段
   * @param {*} meta 元数据
   * @param {*} dragInfo 拖拽信息
   */
  const _setRemove = (dragInfo) => {
    const col = meta.itemMeta[dragInfo.sourceId]
    // 调用外部的确认对话框
    deleteTh(col, () => {
      // 确认移除，才会执行回调
      meta.colOrder.splice(dragInfo.sourceIndex, 1)
      nextTick(() => {
        girdSetup() // 重新绘制
      })
    })
  }

  /**
   * 设置th的对齐方式
   */
  const _setThAlgin = (meta, dragInfo) => {
    // 判断 th 还是 td
    const alignKind = (dragInfo.ctrl) ? 'header-align' : 'align'
    const col = meta.itemMeta[dragInfo.sourceId]
    // 判断：左中右
    switch (col[alignKind]) {
      case 'left':
        if (dragInfo.isLeft) {
          col[alignKind] = 'left'
        } else {
          col[alignKind] = 'center'
        }
        break
      case 'center':
        if (dragInfo.isLeft) {
          col[alignKind] = 'left'
        } else {
          col[alignKind] = 'right'
        }
        break
      case 'right':
        if (dragInfo.isLeft) {
          col[alignKind] = 'center'
        } else {
          col[alignKind] = 'right'
        }
        break
    }
  }

  // 调整 th 的宽度后，记录新的宽度
  const _setThWidth = (e) => {
    // 等待刷新
    setTimeout(() => {
      // 监听事件，获取调整后的 th 的宽度
      // for (let i = 1; i < tdCount; i++) {
      const arr = Array.from(table.rows[0].cells)
      let i = -1
      arr.forEach(element => {
        if (i === -1) { // 跳过第一列
          i = 0
        } else {
          meta.itemMeta[meta.colOrder[i++]].width = element.offsetWidth
        }
      })
    }, 1000)
  }

  /**
   * 列表的初始化设置
   */
  const girdSetup = () => {
    // 调整 th 的宽度后，记录新的宽度
    table.addEventListener("mouseup", _setThWidth)

    // 找到 可以拖拽的 dom，这里是 table 的 tr[0] 的 th 集合
    // const className = 'el-table__header'
    // const table = el.getElementsByClassName(className)[0]
    const tr = table.rows[0] // 表头
    const tdCount = tr.cells.length

    // 拖拽信息
    const dragInfo = {
      timeout: null, // 设置拖拽出去的 setTimeout
      oldDom: {style:{border:''}}, // 记录之前的拖拽 dom
      state: '', // 拖拽状态，拖拽中（pending），拖拽结束（end）
      offsetX: 0,
      isLeft: true, // 是否在 th 的左侧结束拖拽，左侧或者右侧。
      ctrl: false, // 是否按下了ctrl
      sourceId: '', // 开始拖拽的th对应的字段ID
      targetId: '', // 结束拖拽的th对应的字段ID
      sourceIndex: 0, // 开始拖拽的位置
      targetIndex: 0 // 结束拖拽的位置
    }
    // 遍历表头，设置 th 的拖拽，第一列是复选框，不算
    for (let i = 1; i < tdCount; i++) {
      const th = tr.cells[i]
      const colId = meta.colOrder[i - 1]
      // 给 th 设置拖拽事件
      const thDrag = new domDrag(th, meta, dragInfo, colId, table)
      // 设置对齐
      thDrag.setAlign((_dragInfo) => {
        _setThAlgin(meta, _dragInfo)
      })
      // 设置排序
      thDrag.setOrder((_dragInfo) => {
        setOrder(meta, dragInfo) // 调整顺序、交换位置
        nextTick(() => {
          girdSetup() // 重新绘制
        })
      })
      // 移除th
      thDrag.deleteDom((_dragInfo) => {
        _setRemove(dragInfo) // 移除
      })
    }
    document.ondrop = (event) => {
    }
  }

  return {
    girdSetup // 列表设置拖拽
  }

}