
import { reactive } from 'vue'

/**
 * @function 创建表单的 v-model
 * @description 依据表单的 itemMeta，自动创建 model
 * @param { object } meta 表单子控件的meta
 */
export const createFindModel = (meta) => {
  const findModel = reactive({})

  // 依据 meta，创建 model
  for(const key in meta) {
    const m = meta[key]
    findModel[m.columnId] = {
      dateKind: 0, // 查询的时候，使用的日期类型。0：不是日期类型
      findKind: [], // 该字段可以使用的查询方式
      useKind: 401, // 默认查询方式、用户选择的查询方式
      value: '', // 查询值
      value2: '' // 范围查询的结束值
    }

    // 根据控件类型设置属性值
    switch (m.controlType) {
      case 100: // 文本类
      case 101:
      case 102:
      case 103:
      case 104:
      case 105:
      case 106:
      case 107:
      case 130:
      case 131:
        findModel[m.columnId].findKind = [401, 402, 403, 404, 405, 406]
        break
      case 110: // 日期
      case 111: // 日期时间
      case 112: // 年月
      case 114: // 年
      case 113: // 年周
        findModel[m.columnId].findKind = [401, 417, 413, 414, 415, 416]
        break
      case 115: // 任意时间
        findModel[m.columnId].findKind = [401, 417, 413, 414, 415, 416]
        break
      case 116: // 选择时间
        findModel[m.columnId].findKind = [401, 417, 413, 414, 415, 416]
        break
      case 120: // 数字
      case 121:
        findModel[m.columnId].findKind = [401, 417, 413, 414, 415, 416, 441]
        findModel[m.columnId].value = undefined
        findModel[m.columnId].value2 = undefined
        break
      case 150: // 勾选
      case 151: // 开关
        findModel[m.columnId].findKind = [401]
        break
      case 153: // 单选组
      case 160: // 下拉单选
        findModel[m.columnId].findKind = [401, 441]
        break
      case 162: // 下拉联动
        findModel[m.columnId].findKind = [401, 441]
        findModel[m.columnId].value = []
        break
      case 152: // 多选组
      case 161: // 下拉多选
        findModel[m.columnId].findKind = [401, 441]
        findModel[m.columnId].value = []
        break
    }
  }

  return findModel
}