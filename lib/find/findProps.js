
/**
 * 查询控件需要的属性 findProps
 */
 export default {
  moduleId: [Number, String], // 模块ID
  active: Object, // 按钮的meta集合
  colOrder: {
    type: Array,
    default: () => []
  },
  findValue: { // 用户输入的查询条件，紧凑型
    type: Object,
    default: () => {}
  },
  findArray:{ // 用户输入的查询条件，标准型
    type: Object,
    default: () => []
  },
  quickFind: { // 快速查询的字段
    type: Array,
    default: () => []
  },
  allFind: { // 全部查询的字段
    type: Array,
    default: () => []
  },
  customer: { // 自定义的查询方案
    type: Array,
    default: () => {}
  },
  customerDefault: { // 默认的查询方案
    type: Array,
    default: 1
  },
  reload: { // 是否重新加载配置，需要来回取反
    type: Boolean,
    default: false
  },
  itemMeta: { // 表单子控件的属性
    type: Object,
    default: () => {}
  }
}
