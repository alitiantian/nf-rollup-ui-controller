
/**
 * 表单控件需要的属性propsForm
 */
export default {
  moduleId: { // 101 gridID，必填
    type: Number,
    required: true
  },
  height: { // table的高度
    type: Number,
    default: 300
  },
  stripe: { // 斑马纹
    type: Boolean,
    default: true
  },
  border: { // 纵向边框
    type: Boolean,
    default: true
  },
  fit: { // 是否自撑开
    type: Boolean,
    default: true
  },
  highlightCurrentRow: { // 要高亮当前行
    type: Boolean,
    default: true
  },
  colOrder: { // 字段显示的顺序
    type: Array,
    default: () => []
  },
  fixedIndex: { //  锁定的列数
    type: Number,
    default: 0
  },
  idName: { // 主键字段的名称
    type: String,
    default: 'ID'
  },
  itemMeta: Object, // table的列的 meta
  selection: { // 选择的情况
    type: Object,
    default: () => {
      return {
        dataId: '', // 单选ID number 、string
        row: {}, // 单选的数据对象 {}
        dataIds: [], // 多选ID []
        rows: [] // 多选的数据对象 []
      }
    }
  },
  'data-list': { // 绑定的数据
    type: Array,
    default: () => []
  }
}
