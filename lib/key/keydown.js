import { onMounted, onUnmounted } from 'vue'

/**
 * 键盘按键的监听，监听一个按键
 * @param {*} key 要监听的按键
 * @param {*} callback 按键触发后的回调函数。event：按键事件的参数；isAlt：是否按过alt键
 */
export default function mykeydown(key, callback) {
  // 是否按过 alt 
  let isAlt = false
  // 按下的事件
  const butKeydown = (event) => {
    if (event.key === "Alt") {
      isAlt = true
      return
    }
    if (event.key === key) {
      callback(event, isAlt)
    }
    isAlt = false
  }

  const butKeyup =  (event) => {
    if (event.key === "Alt") {
      // 抬起了 alt
      isAlt = false
    }
  }

  // 监听事件
  onMounted((info) => {
    // 创建
    document.addEventListener("keydown", butKeydown)
    // document.addEventListener("keyup", butKeyup)
  })
 
  // 撤销监听
  onUnmounted((info) => {
    // 卸载
    document.removeEventListener("keydown", butKeydown)
    // document.removeEventListener("keyup", butKeyup)
  })
}