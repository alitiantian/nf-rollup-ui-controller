
// 表单子控件的属性
import itemProps from './base/itemProps.js'
// 表单控件的属性
import formProps from './form/formProps.js'
// 查询控件的属性
import findProps from './find/findProps.js'
// 列表控件的属性
import gridProps from './grid/gridProps.js'

// 不用防抖的输入
import custmerRef from './base/custmerRef.js'
// 防抖输入
import debounceRef from './base/debounceRef.js'
// 日期类型的输入
import dateRef from './base/dateRef.js'

// 表单子控件的管理类
import itemController from './base/itemController.js'
// 创建表单需要的 v-model
import { createModel } from './form/modelController.js'
// 表单控件的控制类
import formController from './form/formController.js'

// 创建查询需要的 内部存储结构
import { createFindModel } from './find/modelController.js'
// 查询方式的字典
import { findKindDict } from './find/findKindDict.js'
// 查询控件的控制类
import findController from './find/findController.js'

// 列表
import createDataList from './grid/modelController.js'

// 监听按键
import mykeydown from './key/keydown.js'
import mykeypager from './key/keypager.js'

// 拖拽 dialog
import dialogDrag from './drag/dialogDrag.js'
// 设置 grid的拖拽
import gridDrag from './drag/gridDrag.js'

// 设置表单的拖拽
import formDrag from './drag/formDrag.js'

// 设置 td、div可以拖拽
import _domDrag from './drag/controller/domDrag.js'

// 生命周期
import lifecycle from './base/lifecycle.js'

// 简易路由
import { createRouter, useRouter } from './router/router.js'

// 套个函数，避免 new
const domDrag = (th, meta, dragInfo, colId) => new domDrag(th, meta, dragInfo, colId)


export {
  // 生命周期
  lifecycle,
  // 简易路由
  createRouter,
  useRouter,
  // 属性
  itemProps, // 表单子控件的共用属性
  formProps, // 表单控件的属性
  findProps, // 表单控件的属性
  gridProps, // 列表控件的属性
  // 表单
  createModel, // 创建表单需要的 v-model
  formController, // 表单控件的控制类
  itemController, // 表单子控件的控制类
  // 查询
  findKindDict, // 查询方式的字典
  createFindModel, // 创建查询需要的 内部存储结构
  findController, // 查询控件的控制类
  // 列表
  createDataList, // 建立演示数据
  // 按键
  mykeydown, // 操作按钮
  mykeypager, // 翻页的按键
  // 拖拽
  dialogDrag, // 拖拽 dialog
  gridDrag, // 数据列表的拖拽设置
  formDrag, // 设置表单的拖拽
  domDrag, // 设置 td、div可以拖拽
  // 赋值
  debounceRef, // 防抖
  dateRef, // 处理日期
  custmerRef // 不防抖
}