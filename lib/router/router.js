import { defineAsyncComponent, reactive, watch, inject } from 'vue'

const flag = Symbol('nf-router-menu___')

/**
 * 一个简单的路由
 * @param {*} baseUrl 基础路径
 * @param {*} routes 路由设置
 * * {
 * *   base_html: { // 路由的 name
 * *     path: '/base-html',
 * *     title: '原生HTML',
 * *     icon: Edit,
 * *     component: () => import('./ui/base/c-01html.vue')
 * *   },
 * *   其他路由设置 
 * * }
 * @returns 
 */
class Router {
  constructor (info) {
    // 设置当前选择的路由
    this.currentRoute = reactive({
      key: 'home'
    })
    this.baseUrl = info.baseUrl // 基础路径，应对网站的二级目录
    this.home = info.home // 默认的首页
    this.group = info.group // 一级菜单，分组，设置包含的二级菜单（路由）
    this.routes = info.routes // 路由设置，二级菜单
    /**
     * 'key', 'key' // 保存路由的key（name）
     */
    this.tabs = reactive(new Set([])) // 点击过且没有关闭的二级菜单，做成动态tab标签
    // 把路由里的组件转换一下
    this.routerControl = {}
    this.setup()
  }
  /**
   * 初始化设置
   */
  setup = () => {
    // 监听当前路由，设置 tabs
    watch(() => this.currentRoute.key, (key) => {
      if (key !== 'home' ) this.tabs.add(key)
      const route = this.routes[key] ?? {title: '首页', path: '/'}
      // 设置标题
      document.title = route.title
      // 设置url地址
      window.history.pushState(null, null, this.baseUrl + route.path)
    }, {immediate: true})
    // 把路由里的组件转换一下
    for (const key in this.routes) {
      const r = this.routes[key]
      this.routerControl[key] = defineAsyncComponent(r.component)
    }
  }
  // 加载路由指定的组件
  getComponent = () => {
    if (this.currentRoute.key === '' || this.currentRoute.key === 'home') {
      return this.home
    } else {
      const route = this.routes[this.currentRoute.key]
      if (route) {
        // 返回组件
        return defineAsyncComponent(route.component)
      }
    }
  }

  // 删除tab
  removeTab = (key) => {
    // 转换为数组，便于操作
    const arr = Array.from(this.tabs)
    
    if (arr.length === 1) {
      // 只有一个tab，删除后激活桌面
      this.tabs.delete(key)
      this.currentRoute.key = 'home'
      return
    }

    // 判断是否当前tab，如果是当前tab，激活左面或者右面的tab
    if (this.currentRoute.key === key) {
      // 查找当前tab的数组序号
      const index = arr.indexOf(key)
      // 判断当前tab的位置
      if (index === 0) {
        // 第一位，激活后面的
        this.currentRoute.key = arr[1]
      } else {
        // 激活前面的
        this.currentRoute.key = arr[index - 1]
      }
    }
    // 删除
    this.tabs.delete(key)
  }

  // 刷新时依据url加载组件
  refresh = () => {
    const path = window.location.pathname
    if (path === '/' || path === this.baseUrl) {
      // 首页
    } else {
      const tmp = path.replace(this.baseUrl, '')
      // 验证路由
      for (const key in this.routes) {
        const route = this.routes[key]
        if (route.path === tmp) {
          this.currentRoute.key = key
          break
        }
      }
    }
  }
}

/**
 * 创建简易路由
 */
const createRouter = (info) => {
  // 创建路由，
  const router = new Router(info)
  // 判断url，是否需要加载组件
  // router.refresh()
  // 使用vue的插件，设置全局路由
  return (app) => {
    // 便于模板获取
    app.config.globalProperties.$router = router
    // 便于代码获取
    app.provide(flag, router)
  }
}

const useRouter = () => {
  return inject(flag)
}

export {
  createRouter,
  useRouter
}