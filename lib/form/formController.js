/**
 * 表单控件的控制类
 */
import { reactive, ref, watch, onMounted } from 'vue'

/**
 * 处理一个字段占用几个 位子 的需求
 * @param { object } props 表单组件的属性
 * @returns 子控件占的份数情况（对象）
 */
const getColSpan = (props) => {
  // 确定一个组件占用几个格子
  const formColSpan = reactive({})

  // 根据配置里面的colCount，设置 formColSpan
  const setFormColSpan = () => {
    const formColCount = props.formColCount // 列数
    const moreColSpan = 24 / formColCount // 一个格子占多少份
    // 表单子控件的属性
    const formItemProps = props.itemMeta

    if (formColCount === 1) {
      // 一列的情况
      for (const key in formItemProps) {
        const m = formItemProps[key]
        if (typeof m.colCount === 'undefined') {
          formColSpan[m.columnId] = moreColSpan
        } else {
          if (m.colCount >= 1) {
            // 单列，多占的也只有24格
            formColSpan[m.columnId] = moreColSpan
          } else if (m.colCount < 0) {
            // 挤一挤的情况， 24 除以 占的份数
            formColSpan[m.columnId] = moreColSpan / (0 - m.colCount)
          }
        }
      }
    } else {
      // 多列的情况
      for (const key in formItemProps) {
        const m = formItemProps[key]
        if (typeof m.colCount === 'undefined') {
          formColSpan[m.columnId] = moreColSpan
        } else {
          if (m.colCount < 0 || m.colCount === 1) {
            // 多列，挤一挤的占一份
            formColSpan[m.columnId] = moreColSpan
          } else if (m.colCount > 1) {
            // 多列，占的格子数 * 份数
            formColSpan[m.columnId] = moreColSpan * m.colCount
          }
        }
      }
    }
  }

  // 监听列数，变化后重新设置
  watch(() => props.formColCount, (v1) => {
    setFormColSpan()
  }, { immediate:true })

  return {
    formColSpan,
    setFormColSpan
  }
}

/**
 * 设置控件的联动
 * @param {*} props 表单组件的属性
 * @returns 可见的字段的集合（对象）
 */
const setControlShow = (props) => {
  
  // 设置字段的是否可见
  const showCol = reactive({})
  // 依据meta设置，默认都可见
  if (typeof props.itemMeta !== 'undefined') {
    for (const key in props.itemMeta) {
      showCol[key] = true
    }
  }

  const setFormColShow = () => {
    // 数据变化，联动组件的显示
    if (typeof props.formColShow !== 'undefined') {
      for (const key in props.formColShow) {
        const ctl = props.formColShow[key]
        const colName = props.itemMeta[key].colName
        // 监听组件的值，有变化就重新设置局部model
        watch(() => props.model[colName], (v1, v2) => {
          if (typeof ctl[v1] === 'undefined') {
           
          } else {
            // 设置部分的 model
            // createPartModel(ctl[v1])
            for (const key in props.itemMeta) {
              showCol[key] = false
            }
            ctl[v1].forEach(id => {
              showCol[id] = true
            });
          }
        },
        { immediate: true })
      }
    }
  }
  setFormColShow()

  return {
    showCol,
    setFormColShow
  }
}

/**
 * 创建表单的验证规则，把字段ID转换为字段名称
 * @param { object } props 表单组件的属性
 * @returns
 */
const getRules = (props) => {
  const rules = {}

  // 表单子控件的属性，key是字段ID
  const itemMeta = props.itemMeta

  for (const key in props.ruleMeta) {
    const rule = props.ruleMeta[key]
    rules[itemMeta[key].colName] = rule
  }

  return rules
}

/**
 * @function 表单控件的管理类
 * @description 创建 v-model，创建局部model，设置行列、排序相关的处理
 * @param { object } props 组件参数
 * @param { object } context 上下文
 * @returns { function } 表单管理类
 * * formModel 表单v-model
 * * 创建v-model
 * * 调整列数
 * * 合并
 */
export default function formController (props, context) {
  // 设置 model 的 属性
  if (Object.keys(props.model).length === 0) {
    // 根据子控件的meta，设置默认的model
    Object.assign(props.model, createModel(props.itemMeta))
  }

  // 关于一个字段占用几个td的问题
  const {
    formColSpan, // 一个字段占几个td的对象 
    setFormColSpan // 设置的函数
  } = getColSpan(props)

  // 设置字段的是否可见，控件的联动
  const {
    showCol,
    setFormColShow
  } = setControlShow(props)

  // 设置验证规则，把字段ID转换为字段名称
  const rules = getRules(props)

  // 返回重置函数
  props.events.reset = () => {
    // alert('表单内部修改外部函数')
    setFormColShow()
  }


  return {
    formColSpan, // 确定组件占位
    formRules: rules, // 表单的验证规则
    showCol, // 是否显示字段的对象
    setFormColShow // 设置组件联动
  }
}
