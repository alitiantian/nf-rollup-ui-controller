
/**
 * 表单控件需要的属性
 */
export default {
  model: Object, // 表单的完整的 model
  partModel: Object, // 根据选项过滤后的 model
  // 表单字段的排序、显示依据
  colOrder: { type: Array, default: () => { return []} },
  // 表单的列数
  formColCount: { type: Number, default: 1 },
  // 标签的后缀
  labelSuffix: { type: String, default: '：' },
  // 标签的宽度
  labelWidth: { type: String, default: '130px' },
  // 控件的规格
  size: { type: String, default: 'mini' },
  // 表单子控件的属性
  itemMeta: { type: Object, default: () => { return {}} },
  // 验证信息
  ruleMeta: { type: Object, default: () => { return {}} },
  // 子控件的联动关系
  formColShow: { type: Object, default: () => { return {}} },
  /*
  * 自定义子控件 key:value形式
  * * key: 编号。1：插槽；100-200：保留编号
  * * value：string：标签；函数：异步组件，类似路由的设置
  */
  customerControl: { type: Object, defaule: () => { return {}} },
  // 是否重新加载配置，需要来回取反
  reload: { type: Boolean, default: false },
  reset: { type: Function, default: () => {} },
  events: { type: Object, default: () => { return {}} }
}
