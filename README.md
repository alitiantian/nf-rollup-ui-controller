# nf-ui-controller-UI库的二次封装的共用 JavaScript 函数

> 源码目录：https://naturefw.gitee.io/  

#### 介绍
UI库，二次封装用的共用 js 函数库，打算支持各种UI库。

## 目录结构

* lib 状态管理的源码
* src 状态管理的使用demo
* distp 在线演示的代码

#### 安装教程

```
yarn add nf-ui-controller
```

#### 依赖的资源包（需要手动安装）

* dayjs
* nf-tool


#### 源码
https://gitee.com/naturefw/nf-rollup-ui-controller

[![自然框架/nf-ui-controller-UI库的二次封装的共用 JavaScript 函数](https://gitee.com/naturefw/nf-rollup-ui-controller/widgets/widget_card.svg?colors=ffffff,1e252b,323d47,455059,d7deea,99a0ae)](https://gitee.com/naturefw/nf-rollup-ui-controller)

#### 在线演示
https://naturefw.gitee.io/nf-rollup-ui-controller/


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
