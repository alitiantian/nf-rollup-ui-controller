import { Loading, Connection, Edit, FolderOpened } from '@element-plus/icons'
import { createRouter } from '/nf-ui-core'
import home from '../views/home.vue'

export default createRouter({
  /**
   * 基础路径
   */
  baseUrl: '/nf-rollup-ui-controller',
  /**
   * 首页
   */
  home: home,
  /**
   * 一级菜单，设计包含哪些二级菜单
   */
  group: [
    {
      id: 1,
      title: '基础控件',
      icon: FolderOpened,
      children: [
        'base_html',
        'base_ui',
        'base_transmit',
        'base_item'
      ]
    },
    {
      id: 2,
      title: '复合控件',
      icon: FolderOpened,
      children: [
        'nf_form',
        'nf_find',
        'nf_grid',
        'nf_button',
        'nf_crud'
      ]
    }
  ],

  /**
   * 二级菜单，设置路径、标题、图标和加载的组件
   */
  routes: {
    base_html: {
      path: '/base-html', title: '原生HTML', icon: Edit,
      component: () => import('../views/ui/base/c-01html.vue')
    },
    base_ui: {
      path: '/base-ui', title: 'UI库组件', icon: Edit,
      component: () => import('../views//ui/base/c-02UI.vue')
    },
    base_transmit: {
      path: '/base-transmit', title: '传声筒', icon: Edit,
      component: () => import('../views//ui/base/c-03transmit.vue')
    },
    base_item: {
      path: '/base-item', title: '基础组件', icon: Edit,
      component: () => import('../views//ui/base/c-06item.vue')
    },
    nf_form: {
      path: '/nf-form', title: '表单控件', icon: Edit,
      component: () => import('../views//ui/form/index.vue')
    },
    nf_find: {
      path: '/nf-find', title: '查询控件', icon: Edit,
      component: () => import('../views//ui/find/index.vue')
    },
    nf_grid: {
      path: '/nf-grid', title: '列表控件', icon: Edit,
      component: () => import('../views//ui/grid/index.vue')
    },
    nf_button: {
      path: '/nf-button', title: '操作按钮', icon: Connection,
      component: () => import('../views//ui/button/index.vue')
    },
    nf_crud: {
      path: '/nf-crud', title: '增删改查', icon: Connection,
      component: () => import('../views//ui/crud/index.vue')
    }
  }
})