import { createApp } from 'vue'
import App from './App.vue'

// UI库
import ElementPlus from 'element-plus'
// import 'element-plus/lib/theme-chalk/index.css'
import 'dayjs/locale/zh-cn'
// import locale from 'element-plus/lib/locale/lang/zh-cn'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

// 实现拖拽的自定义指令
import { nfElementPlus } from '/nf-ui-elp'

// 简易路由
import router from './router'

createApp(App)
  .use(nfElementPlus) // 全局注册
  .use(router) // 路由
  // .use(ElementPlus, { locale: zhCn, size: 'small' }) // UI库
  .mount('#app')
