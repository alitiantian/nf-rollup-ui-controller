
/**
 * 做一个中转，方便内部引用
 */
export {
  itemProps, // 表单子控件的共用属性
  // 表单
  formProps, // 表单控件的属性
  createModel, // 创建表单需要的 v-model
  formController, // 表单控件的控制类
  itemController, // 表单子控件的控制类
  // 查询
  findProps, // 查询控件的属性
  findKindDict, // 查询方式的字典
  createFindModel, // 创建查询需要的 内部存储结构
  findController, // 查询控件的控制类
  // 列表
  gridProps, // 列表控件的属性
  createDataList, // 建立演示数据
  // 按键
  mykeydown, // 操作按钮
  mykeypager, // 翻页的按键
  // 拖拽
  dialogDrag, // 拖拽 dialog
  gridDrag, // 数据列表的拖拽设置
  formDrag, // 设置表单的拖拽
  domDrag, // 设置 td、div可以拖拽
  // 赋值
  debounceRef, // 防抖
  dateRef, // 处理日期
  custmerRef // 不防抖
} from '../lib/main.js'