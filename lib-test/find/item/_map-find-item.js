// 文本
import nfText from './f-text.vue' // 101
// 数字
import nfNumber from './f-number.vue' // 120
// 日期
import nfDate from './f-date.vue' // 120
// 下拉
import nfSelect from './f-list.vue' // 160
// radio
import nfRadios from './f-radios.vue' // 150

export const findItem = {
  // 文本
  nfText,
  // 数字
  nfNumber,
  // 日期时间
  nfDate,
  // 选择，多选
  nfRadios,
  // 下拉
  nfSelect
}

export const findItemDict = {
  // 文本类
  100: 'nf-text',
  101: 'nf-text',
  102: 'nf-text',
  103: 'nf-text',
  104: 'nf-text',
  105: 'nf-text',
  106: 'nf-text',
  107: 'nf-text',
  // 数字
  120: 'nf-number',
  121: 'nf-number',
  // 日期
  110: 'nf-date',
  111: 'nf-datetime',
  112: 'nf-month',
  113: 'nf-week',
  114: 'nf-year',
  115: 'nf-time-picker',
  116: 'nf-time-select',
  // 平铺选择
  150: 'nf-radios',
  151: 'nf-radios',
  152: 'nf-checkboxs',
  153: 'nf-radios',
  // 下拉选项
  160: 'nf-select',
  161: 'nf-selects',
  162: 'nf-select-cascader',
  163: 'nf-select-level'
}