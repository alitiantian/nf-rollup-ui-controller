// 操作按钮
import nfButton from './button/button.vue'

// 表单
import nfForm from './form/form-div.vue'
import nfFormCard from './form/form-div-card.vue'
import { formItem, formItemKey } from './form/item/_map-form-item.js'

// 查询
import nfFind from './find/find-div.vue'

// 列表
import nfGrid from './grid/grid-table.vue' // './grid/grid-table.vue'

// 拖拽
import { _dialogDrag, install as dialogDrag } from './drag/dialogDrag.js'
// import findDrag from './drag/findDrag.js'
import { _formDrag, install as formDrag } from './drag/formDrag.js'
import { _gridDrag, install as gridDrag} from './drag/girdDrag.js'

// 菜单
import nfMenu from './menu/menu.vue'
// 路由视图
import routerView from './menu/router-view.vue'
import routerViewTabs from './menu/router-view-tabs.vue'

import loading from './js/loading.js'

// 全局注册组件和自定义指令
const nfElementPlus = (app) => {
  // 按钮
  app.component('nfButton', nfButton),
  // 表单
  app.component('nfForm', nfForm),
  app.component('nfFormCard', nfFormCard),
  // 查询
  app.component('nfFind', nfFind),
  // 列表
  app.component('nfGrid', nfGrid)
  // 菜单
  app.component('nfMenu', nfMenu)
  // 路由视图
  app.component('routerView', routerView)
  app.component('routerViewTabs', routerViewTabs)

  // 表单子控件
  for (const key in formItem) {
    app.component(key, formItem[key])
  }
  // 自定义指令
  app.directive('dialogDrag', _dialogDrag)
  app.directive('formDrag', _formDrag)
  app.directive('gridDrag', _gridDrag)

}

export {
  nfElementPlus, // 安装插件
  loading, // 加载动画
  dialogDrag, // 拖拽对话框
  // findDrag,
  formDrag,
  gridDrag,
  // 菜单
  nfMenu,
  // 路由视图
  routerView,
  routerViewTabs,
  // 表单
  nfForm, // 表单控件
  nfFormCard, // 分组的表单控件
  formItem, // 表单子控件集合
  // 查询
  nfFind, // 查询控件
  // 列表
  nfGrid, // 列表控件
  nfButton // 操作按钮
}