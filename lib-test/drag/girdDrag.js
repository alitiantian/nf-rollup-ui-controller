import { nextTick } from 'vue'
import { gridDrag } from '../mapController.js'
import { ElMessageBox, ElMessage } from 'element-plus'

const msgBox = {
  confirmButtonText: '移除', // + dataListState.choice.dataId,
  cancelButtonText: '不要呀',
  type: 'warning'
}
const msgSucess = {
  type: 'success',
  message: '移除成功！'
}
const msgError = {
  type: 'info',
  message: '已经取消了。'
}

// 移除 dom 触发的事件，弹出对话框确认，
const deleteDom = (col, del) => {
  ElMessageBox.confirm(`此操作将移除该列：(${col.label}), 是否继续？`, '温馨提示', msgBox)
    .then(() => {
      // 移除
      del()
    })
    .catch(() => {
      ElMessage(msgError)
    })
}

const _gridDrag = {
  // 指令的定义
  mounted (el, binding) {
    // console.log('===  griddrag == binding', binding)
    const className = 'el-table__header'
    // 控件的meta
    const meta = binding.value
    // 渲染的比较慢
    nextTick(() => {
      const table = el.getElementsByClassName(className)[0]
      // const tr = table.rows[0] // 表头
      // const tdCount = tr.cells.length

      gridDrag(meta, table, deleteDom).girdSetup()
    
    })
    
  },

  // 移除事件
  unmounted(el, binding) {

  }
}
/**
 * grid 的拖拽的自定义指令，
 * * 可以记录调整后的 td 的宽度，
 * * 修改 td 的顺序，
 * * 移除 td
 * @param {*} app 
 * @param {*} options 
 */
const install = (app, options) => {
  app.directive('gridDrag', _gridDrag)
}

export {
  _gridDrag,
  install
}
