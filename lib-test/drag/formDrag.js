import { nextTick } from 'vue'
import { formDrag } from '../mapController.js'
import { ElMessageBox, ElMessage } from 'element-plus'

const msgBox = {
  confirmButtonText: '移除', // + dataListState.choice.dataId,
  cancelButtonText: '不要呀',
  type: 'warning'
}
const msgSucess = {
  type: 'success',
  message: '移除成功！'
}
const msgError = {
  type: 'info',
  message: '已经取消了。'
}

// 移除 dom 触发的事件，弹出对话框确认，
const deleteDom = (col, del) => {
  ElMessageBox.confirm(`此操作将移除该列：(${col.label}), 是否继续？`, '温馨提示', msgBox)
    .then(() => {
      // 移除
      del()
    })
    .catch(() => {
      ElMessage(msgError)
    })
}

const _formDrag = {
  // 指令的定义
  mounted (el, binding) {
    // console.log('===  formdrag == binding', binding)
    // 控件的meta
    const meta = binding.value
    // 渲染的比较慢
    nextTick(() => {
      const form = el.children[0]

      formDrag(meta, form, deleteDom).setup()
    
    })
    
  },

  // 移除事件
  unmounted(el, binding) {

  }
}
/**
 * form 表单控件 的拖拽的自定义指令，
 * * 修改 字段 的顺序，
 * * 移除 字段
 * @param {*} app 
 * @param {*} options 
 */
const install = (app, options) => {
  app.directive('formDrag', _formDrag)
}

export {
  _formDrag,
  install
} 
