import { dialogDrag } from '../mapController.js'

const _dialogDrag = {
  // 指令的定义
  mounted (el, binding) {
    // console.log('===  dialogDrag == binding', binding)
  
    // 蒙版
    const container = el.firstElementChild.firstElementChild
    // 弹窗的容器
    const dialog = el.firstElementChild.firstElementChild.firstElementChild
    // 拖拽的div
    const dialogTitle = el.firstElementChild.firstElementChild.firstElementChild.firstElementChild
    // dialog 的宽度
    const width = binding.value.replace('%', '') //

    const { setDialog } = dialogDrag()
    // 设置
    setDialog(container, dialog, dialogTitle, width)
  },

  // 移除事件
  unmounted(el, binding) {
    // 蒙版
    const container = el.firstElementChild.firstElementChild
    // 拖拽的div
    const dialogTitle = el.firstElementChild.firstElementChild.firstElementChild.firstElementChild
    const { unload } = dialogDrag()
    // 设置
    unload(container, dialogTitle)
  }
}

/**
 * element-plus 的 dialog 的拖拽的自定义指令
 * @param {*} app 
 * @param {*} options 
 */
const install = (app, options) => {
  app.directive('dialogDrag', _dialogDrag)
}

export {
  _dialogDrag,
  install
} 
