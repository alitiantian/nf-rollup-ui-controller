
import { nextTick } from 'vue'

import { ElLoading } from 'element-plus'

let loadingInstance1 = null

const loading = (id) => {

  const start = () => {
    loadingInstance1 = ElLoading.service({
      text: '加载中...',
      background: 'rgba(255, 255, 255, 0.8)',
      target: id
    })
  }

  const stop = () => {
    loadingInstance1.close()
  }

  nextTick(() => {
    if (loadingInstance1)
      loadingInstance1.close()
  })

  return {
    start,
    stop
  }
}

export default loading