// 文本
import nfArea from './t-area.vue' // 100
import nfText from './t-text.vue' // 101
import nfPassword from './t-password.vue' // 102
import nfUrl from './t-url.vue' // 105
import nfColor from './t-color.vue' // 107
import nfAutocomplete from './t-autocomplete.vue' // 109
// 数字
import nfNumber from './n-number.vue' // 120
import nfRange from './n-range.vue' // 121
import nfRate from './n-rate.vue'
// 日期
import nfDate from './d-date.vue' // 110
import nfDatetime from './d-datetime.vue' // 111
import nfMonth from './d-month.vue' // 112
import nfTimePicker from './d-time-picker.vue' // 115
import nfTimeSelect from './d-time-select.vue' // 116
import nfWeek from './d-week.vue' // 113
import nfYear from './d-year.vue' // 114

// 二选一
import nfSwitch from './s1-switch.vue' // 150
import nfCheckbox from './s1-checkbox.vue' // 151
// 选择
import nfCheckboxs from './s2-checkboxs.vue' // 152
import nfRadios from './s2-radios.vue' // 153
// 下拉
import nfSelect from './s-select.vue' // 160
import nfSelects from './s-selects.vue' // 162
import nfSelectCascader from './s-select-cascader.vue' // 162

export const formItem = {
  // 文本
  nfText,
  nfArea,
  nfUrl,
  nfPassword,
  nfColor,
  nfAutocomplete,
  // 数字
  nfNumber,
  nfRange,
  nfRate,
  // 日期时间
  nfDate,
  nfDatetime,
  nfMonth,
  nfTimePicker,
  nfTimeSelect,
  nfWeek,
  nfYear,
  // 选择
  nfSwitch,
  nfCheckbox,
  nfCheckboxs,
  nfRadios,
  // 下拉
  nfSelect,
  nfSelectCascader,
  nfSelects
}

export const formItemKey = {
  // 文本类
  100: 'nf-area',
  101: 'nf-text',
  102: 'nf-password',
  103: 'nf-text',
  104: 'nf-text',
  105: 'nf-url',
  106: 'nf-text',
  107: 'nf-autocomplete',
  // 数字
  120: 'nf-number',
  121: 'nf-range',
  // 日期
  110: 'nf-date',
  111: 'nf-datetime',
  112: 'nf-month',
  113: 'nf-week',
  114: 'nf-year',
  115: 'nf-time-picker',
  116: 'nf-time-select',
  // 选择等
  150: 'nf-checkbox',
  151: 'nf-switch',
  152: 'nf-checkboxs',
  153: 'nf-radios',
  160: 'nf-select',
  161: 'nf-selects',
  162: 'nf-select-cascader',
  163: 'nf-select-level'
}