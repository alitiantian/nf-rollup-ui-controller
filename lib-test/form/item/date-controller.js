import { ref, watch } from 'vue'
import { dateRef } from '../../mapController.js'

/**
 * 日期管理类
 * * 功能：
 * ** 按照类型提交数据，不是date()
 * ** 监听属性，设置value
 * * 参数：
 * ** value： control类的value
 * ** mySubmit： control类的mySubmit，直接就提交了
 * ** controlType：属性里的控件类型
 * * 返回
 * ** 绑定控件的 mydate
 * ** change 事件的 myChange  value instanceof Date
 */
export default function (props, emit) {
  const ctlEvents = {
    run: () => {}, // 立即执行，不防抖了
    clear: () => {} // 清除上一次计时，用于输入汉字的情况。
  }
  const value = dateRef(props, ctlEvents, emit)

  return {
    value
  }
}
